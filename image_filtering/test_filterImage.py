import filterImage

PATH = "./images/"

fileList = ["all_dark_bag.jpg",
            "light_back.jpg",
            "closer_light.jpg",
            "red_image0.jpg",
            "red_image3.jpg",
            "black_01.jpg",
            "black_02.jpg",
            "black_03.jpg",
            "black_04.jpg",
            "black_2.jpg",
            "earth_01.jpg",
            "earth_02.jpg",
            "earth_03.jpg",
            "earth_04.jpg",
            "leaf.jpg",
            "result.jpg",
            "scene.jpg", ]

negativeFileList = [PATH + "black_01.jpg", PATH +
                    "black_02.jpg", PATH + "all_dark_bag.jpg"]
'''
Tests the filterImage function.
  If any image has any small image atleast, it needs to 
  decide to send the image.
'''


def testfilterImage(fileList, PATH):
    print("Starting test...")
    for file in fileList:
        file = PATH + file
        if file in negativeFileList:
            if not filterImage.filterImage(file):
                print("Test passed for negative sample...")
            else:
                print("Test failed for negative sample: ", file)
        else:
            if(filterImage.filterImage(file)):
                print("Test passed for positive sample...")
            else:
                print("Test failed for positive sample: ", file)


if __name__ == '__main__':

    testfilterImage(fileList, PATH)
