#!/usr/bin/env python3
# Copyright (C) 2018 Quick2Space.org under the MIT License (MIT)
# See the LICENSE.txt file in the project root for more information.

import tkinter as tk
import picamera as pc
import picamera.array as pcarray
import time
import numpy 


def DrawArray(arr):
    xd = arr.shape[0]
    yd = arr.shape[1]
    
    # 5 pixels width 
    xScaleFactor = 6
    yScaleFactor = 5
    
    top = tk.Tk()
    C = tk.Canvas(top, bg="black", 
        height=yd * yScaleFactor, 
        width=xd * xScaleFactor)

    for x in range(xd):
        for y in range(yd):
            grayShade = arr[x, y] 
            color = "#%02x%02x%02x" % (
                        grayShade, grayShade, grayShade)
            C.create_rectangle(y * yScaleFactor, x * xScaleFactor,
                    (y + 1) * yScaleFactor, (x + 1) * xScaleFactor, 
                    fill=color)

    C.pack()
    top.mainloop()

def CaptureArray():
    with pc.PiCamera() as camera:
        rx = 96
        ry = 64
        camera.resolution = (rx, ry)
        # camera.start_preview()
        # time.sleep(2)
        # camera.stop_preview()
        with pcarray.PiRGBArray(camera) as output:
            camera.capture(output, 'rgb')
            xd = output.array.shape[0]
            yd = output.array.shape[1]
            cd = output.array.shape[2]
            print('Captured %dx%d image (%d color), size = %.2f KB w. color' % (
                    yd, 
                    xd,
                    cd,
                    xd * yd * cd / 1024
                    ))
            # print("Data: ", output.array)

            oa = output.array 
            grayImage = numpy.zeros(shape = (xd, yd), dtype = numpy.uint8)
            for x in range(xd):
                for y in range(yd):
                    # TODO - add better RGB weighting? 
                    grayColor = int((float(oa[x,y,0]) + 
                        float(oa[x,y,1]) + 
                        float(oa[x,y,2]))/3)
                    grayImage[x][y] = grayColor
                    # print(x, y, " ->", (oa[x,y,0], oa[x,y,1], oa[x,y,2]), grayColor)

            # print("----------- Gray vector: ")
            # print(grayImage)
            return grayImage


grayImage = CaptureArray()
DrawArray(grayImage)

